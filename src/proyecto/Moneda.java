package proyecto;

public class Moneda {
    private String nombre = "";
    private float valorEnPesos = 0;
    private String tipo="";
    public Moneda(String nombre, float valorEnPesos) {
        this.nombre = nombre;
        this.valorEnPesos = valorEnPesos;

    }


    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getValorEnPesos() {
        return valorEnPesos;
    }

    public void setValorEnPesos(float valorEnPesos) {
        this.valorEnPesos = valorEnPesos;
    }
}
