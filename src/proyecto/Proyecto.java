package proyecto;


import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JOptionPane;
public class Proyecto {
    public static void main(String[] args) {

        StringBuilder menu = new StringBuilder("\t Menu de opciones");
        ArrayList<Criptomoneda> criptomonedaArrayList = new ArrayList<Criptomoneda>();
        ArrayList<MoendaFisica> moendaFisicaArrayList = new ArrayList<MoendaFisica>();
        criptomonedaArrayList.add(0,new Criptomoneda("Bticoin",1258489.07f));
        criptomonedaArrayList.add(1,new Criptomoneda("Etherium",76095.14f));
        moendaFisicaArrayList.add(0,new MoendaFisica("Dolar",20.04f));
        moendaFisicaArrayList.add(1, new MoendaFisica("Peso",1f));
        Scanner sc = new Scanner(System.in);
        String op ="";
        boolean salir = false;
        Cuenta cuenta = new Cuenta("Jorge");

        do{
            menu.append("Elige una opcion \n");
            menu.append("a. Ver detalles de cuenta \n ");
            menu.append("b. realizar un deposito \n ");
            menu.append("c. Retirar \n ");
            menu.append("d. Ver historial de depositos\n  ");
            menu.append("e. Salir \n" );

            menu.append("Escribe una de las opciones");
            menu.append("Ingresa una opcion  \n");
            op = JOptionPane.showInputDialog(menu);
            menu.setLength(0);
            switch (op.toUpperCase()) {
                case "A":

                    JOptionPane.showMessageDialog(null, cuenta.Detalles());
                    break;
                case "B":
                    StringBuilder menuDeposito = new StringBuilder();
                    String nombreMonedaSelected = "";
                    Moneda monedaSelected = null;
                    Integer opDeposito = Integer.parseInt(JOptionPane.showInputDialog("Elige un tipo de deposito \n" +
                            "1 .- Criptomoneda  \n"  +
                            "2.- Moneda comun  \n"));
                    if (opDeposito ==1){
                        menuDeposito.append("Elige una de las criptomonedas \n");
                        for (int i = 0; i < criptomonedaArrayList.size() ; i++) {
                            menuDeposito.append(i+1).append("  ").append(criptomonedaArrayList.get(i).getNombre()).append(" \n");
                        }
                        Integer criptoSelectedIndex = Integer.parseInt(JOptionPane.showInputDialog(menuDeposito));
                        nombreMonedaSelected= criptomonedaArrayList.get(criptoSelectedIndex-1).getNombre();
                        monedaSelected= criptomonedaArrayList.get(criptoSelectedIndex-1);
                        menuDeposito.setLength(0);
                    }else if (opDeposito ==2){
                        menuDeposito.append("Elige una de las criptomonedas \n");
                        for (int i = 0; i < moendaFisicaArrayList.size() ; i++) {
                            menuDeposito.append(i+1).append("  ").append(moendaFisicaArrayList.get(i).getNombre()).append(" \n");
                        }
                        Integer criptoSelectedIndex = Integer.parseInt(JOptionPane.showInputDialog(menuDeposito));
                        nombreMonedaSelected= moendaFisicaArrayList.get(criptoSelectedIndex-1).getNombre();
                        monedaSelected= moendaFisicaArrayList.get(criptoSelectedIndex-1);
                        menuDeposito.setLength(0);

                    }
                    System.out.println(monedaSelected.getNombre());
                    String motivo = JOptionPane.showInputDialog(null,"Ingresa el motivo de pago");
                    float importe = Float.parseFloat(JOptionPane.showInputDialog(null,"Ingresa el monto de deposito"));
                    Deposito deposito = new Deposito(motivo,importe, nombreMonedaSelected, monedaSelected);
                    cuenta.hacerDeposito(deposito);
                    break;
                case "C":
                    float cantidadAretirar = Float.parseFloat(JOptionPane.showInputDialog("Que cantidad deseas retirar "));
                    cuenta.retirar(cantidadAretirar);
                    break;
                case "D":
                    cuenta.EstadoDecuenta();
                    break;

                case "E":
                    JOptionPane.showMessageDialog(null,"Adios ;)");
                    salir = true;
                    break;
                default:
                    System.out.println("Option incorrecta");
                    break;
            }
        } while(!salir);

    }
}
