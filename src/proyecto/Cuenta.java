package proyecto;

import javax.swing.*;
import java.util.ArrayList;


public class Cuenta {
    String titular ="";
    float dinero =0;

     ArrayList< Deposito> depositos = new ArrayList<>();

    public Cuenta(String titular) {
        this.titular = titular;
    }
    public void hacerDeposito(Deposito deposito){

        if (deposito.moneda.getTipo()=="Cripto") {
            deposito.importe = deposito.importe * deposito.moneda.getValorEnPesos();
            this.dinero += deposito.importe;
        }else{
            deposito.importe = deposito.importe* deposito.moneda.getValorEnPesos();
            this.dinero +=  deposito.importe;
        }

        this.depositos.add(deposito);
    }

    public void EstadoDecuenta(){
        StringBuilder depositos = new StringBuilder("N:   Motivo    Importe     Fecha                   Moenda \n ");
        for (int i = 0; i < this.depositos.size() ; i++) {
            depositos.append(i+1).append("    ").append(this.depositos.get(i).getMotivoDePago());
            depositos.append("     " + "$").append(this.depositos.get(i).getImporte()).append("     ");
            depositos.append(this.depositos.get(i).getFecha()).append("     ");
            depositos.append(this.depositos.get(i).nombreDeMoneda)
                    .append("\n ");

        }
        JOptionPane.showMessageDialog(null, depositos);
    }
    public  void retirar(float cantidad){

        if(cantidad > dinero){
            JOptionPane.showMessageDialog(null,"No tienes suficiente dinero p");
        }else{
            dinero -= cantidad;
        }
    }
    public StringBuilder Detalles() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Cuenta \n ").append("titular  ").append(" Fondos  ").append(" Depositos").append("\n");
        stringBuilder.append(titular.toUpperCase()).append("    ").append(dinero).append("               ").append(depositos.size());

        return stringBuilder;
    }
}
