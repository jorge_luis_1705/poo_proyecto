package proyecto;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Deposito {
    String MotivoDePAgo = "";
    float importe =0;
    String fechaDesposito =  new SimpleDateFormat("dd-MM-yyyy k:m").format(new Date());
    String nombreDeMoneda = "";
    Moneda moneda = null;
    public Deposito(String motivoDePAgo, float importe, String nombreDeMoneda, Moneda moneda) {
        MotivoDePAgo = motivoDePAgo;
        this.importe = importe;
        this.nombreDeMoneda = nombreDeMoneda;
        this.moneda = moneda;


    }
    public String getMotivoDePago() {
        return MotivoDePAgo;
    }

    public void setMotivoDePago(String motivoDePAgo) {
        MotivoDePAgo = motivoDePAgo;
    }

    public float getImporte() {
        return importe;
    }

    public void setImporte(float importe) {
        this.importe = importe;
    }

    public String getFecha() {
        return fechaDesposito;
    }
}
